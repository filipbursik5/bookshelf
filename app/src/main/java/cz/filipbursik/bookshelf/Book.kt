package cz.filipbursik.bookshelf

data class Book(var id: Int = 0, var title: String = "", var thumbnail: String = "", var new: String = "")