package cz.filipbursik.bookshelf

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.bookshelf_activity.*

class BookshelfActivity : AppCompatActivity(), BookshelfView {

    private val presenter = BookshelfPresenter(this, BookshelfModel())

    private val COLUMNS_PORTRAIT = 3
    private val COLUMNS_LANDSCAPE = 4

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bookshelf_activity)

        presenter.loadBooks()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun showError(textError: String) {
        Toast.makeText(this, textError, Toast.LENGTH_LONG).show()
    }

    override fun showBooks(books: List<Book>) {
        if (list == null) return

        val columns = getCountOfColumns()

        list.adapter = BookshelfAdapter(books)
        list.layoutManager = GridLayoutManager(this, columns)
    }

    override fun showLoadingBar() {
        if (loadingBar == null) return

        loadingBar.visibility = View.VISIBLE
    }

    override fun hideLoadingBar() {
        if (loadingBar == null) return

        loadingBar.visibility = View.GONE
    }

    private fun getCountOfColumns(): Int {
        when (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            true -> return COLUMNS_PORTRAIT
            false -> return COLUMNS_LANDSCAPE
        }
    }
}
