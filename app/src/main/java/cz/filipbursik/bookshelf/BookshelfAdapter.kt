package cz.filipbursik.bookshelf

import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import cz.filipbursik.bookshelf.utility.ImageService
import cz.filipbursik.bookshelf.utility.ImageService.ImageDownloadCallback
import kotlinx.android.synthetic.main.bookshelf_item.view.*


class BookshelfAdapter(private val items: List<Book>) :
        RecyclerView.Adapter<BookshelfAdapter.MainViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.bookshelf_item, parent, false) as LinearLayout

        return MainViewHolder(v)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val book: Book = items[position]
        holder.title.text = book.title

        val imageDownloadCallback = object : ImageDownloadCallback {
            override fun onError() {
            }

            override fun onSuccess(bitmap: Bitmap?) {
                holder.picture.setImageBitmap(bitmap)
            }
        }

        ImageService.DownloadImageTask(imageDownloadCallback, book.thumbnail).execute()
    }

    override fun getItemCount(): Int = items.size


    class MainViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.txtTitle
        val picture: ImageView = view.imgPicture
    }
}