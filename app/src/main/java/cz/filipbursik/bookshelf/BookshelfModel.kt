package cz.filipbursik.bookshelf

import android.util.Log
import java.io.*
import android.os.AsyncTask
import cz.filipbursik.bookshelf.utility.HttpHandler
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory

class BookshelfModel {

    interface OnLoginFinishedListener {
        fun onConnectionError()
        fun onServerError()
        fun onSuccess(books: List<Book>)
    }

    fun loadBooks(listener: OnLoginFinishedListener) {
        GetXMLFromServer(listener).execute()
    }

    fun getString(id: Int) : String {
        return getString(id)
    }

    private inner class GetXMLFromServer : AsyncTask<String, Void, String> {

        var nh: HttpHandler = HttpHandler()
        val LOG_TAG = "HttpHandler"
        val NOT_CONNECTED = "HttpHandler"
        val PARSE_ERROR = "HttpHandler"
        val BOOK_STRING = "BOOK"
        val ID_STRING = "ID"
        val TITLE_STRING = "TITLE"
        val THUMBNAIL_STRING = "THUMBNAIL"
        val NEW_STRING = "NEW"

        val books = ArrayList<Book>()
        var onLoginFinishedListener: OnLoginFinishedListener

        constructor(onLoginFinishedListener: OnLoginFinishedListener) : super() {
            this.onLoginFinishedListener = onLoginFinishedListener
        }

        override fun doInBackground(vararg strings: String): String {
            var res = ""
            nh = HttpHandler()
            val `is` = nh.callServer(Config.XML_URL)
            if (`is` != null) {

                res = nh.streamToString(`is`)

            } else {
                res = NOT_CONNECTED
            }

            return res
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)

            if (result == NOT_CONNECTED) {
                onLoginFinishedListener.onConnectionError()
            } else {
                var books = ParseXML(result)
                onLoginFinishedListener.onSuccess(books)
            }
        }

        private fun ParseXML(xmlString: String) : List<Book> {
            try {
                val factory = XmlPullParserFactory.newInstance()
                factory.isNamespaceAware = true
                val parser = factory.newPullParser()
                parser.setInput(StringReader(xmlString))

                var eventType = parser.eventType
                var book: Book? = null

                while (eventType != XmlPullParser.END_DOCUMENT) {

                    if (eventType == XmlPullParser.START_TAG) {

                        val name = parser.name

                        if (name == BOOK_STRING && book != null) {
                            books.add(book)
                            book = Book()
                        } else if (name == BOOK_STRING && book == null) {
                            book = Book()
                        }

                        if (name == ID_STRING) {

                            if (parser.next() == XmlPullParser.TEXT) {
                                val id = parser.text
                                book?.id = id.toInt()
                            }
                        } else if (name == TITLE_STRING) {

                            if (parser.next() == XmlPullParser.TEXT) {
                                val title = parser.text
                                book?.title = title
                            }
                        } else if (name == THUMBNAIL_STRING) {

                            if (parser.next() == XmlPullParser.TEXT) {
                                val thumbnail = parser.text
                                book?.thumbnail = thumbnail
                            }
                        } else if (name == NEW_STRING) {

                            if (parser.next() == XmlPullParser.TEXT) {
                                val new = parser.text
                                book?.new = new
                            }
                        }
                    }

                    eventType = parser.next()
                }
            } catch (e: Exception) {
                Log.d(LOG_TAG, PARSE_ERROR, e)
            }

            return books
        }
    }
}

