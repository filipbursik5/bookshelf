package cz.filipbursik.bookshelf



class BookshelfPresenter(var bookshelfView: BookshelfView?, val bookshelfModel: BookshelfModel) :
        BookshelfModel.OnLoginFinishedListener {

    fun loadBooks() {
        bookshelfView?.showLoadingBar()
        bookshelfModel.loadBooks(this)
    }

    fun onResume() {
    }

    fun onDestroy() {
        bookshelfView = null
    }

    override fun onConnectionError() {
        var error = bookshelfModel.getString(R.string.no_internet_connection)

        bookshelfView?.hideLoadingBar()
        bookshelfView?.showError(textError = error)
    }

    override fun onServerError() {
        var error = bookshelfModel.getString(R.string.server_error)

        bookshelfView?.hideLoadingBar()
        bookshelfView?.showError(textError = error)
    }

    override fun onSuccess(books: List<Book>) {
        bookshelfView?.hideLoadingBar()
        bookshelfView?.showBooks(ArrayList())

        bookshelfView?.showBooks(books)
    }

}
