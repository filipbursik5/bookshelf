package cz.filipbursik.bookshelf

interface BookshelfView {
    fun showBooks(books: List<Book>)
    fun showLoadingBar()
    fun hideLoadingBar()
    fun showError(textError: String)
}