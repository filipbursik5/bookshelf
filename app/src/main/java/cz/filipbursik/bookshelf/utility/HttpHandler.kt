package cz.filipbursik.bookshelf.utility

import android.util.Log
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class HttpHandler {

    val LOG_TAG = "HttpHandler"
    val SERVER_ERROR = "Error in callServer"
    val STREAM_ERROR = "Error in streamToString"

    fun callServer(remoteURL: String): InputStream? {
        var inStm: InputStream? = null

        try {
            val url = URL(remoteURL)
            val cc = url.openConnection() as HttpURLConnection
            cc.readTimeout = 5000
            cc.connectTimeout = 5000
            cc.requestMethod = "GET"
            cc.doInput = true
            val response = cc.responseCode

            if (response == HttpURLConnection.HTTP_OK) {
                inStm = cc.inputStream
            }

        } catch (e: Exception) {
            Log.e(LOG_TAG, SERVER_ERROR, e)
        }

        return inStm

    }

    fun streamToString(stream: InputStream): String {
        val isr = InputStreamReader(stream)
        val reader = BufferedReader(isr)
        val response = StringBuilder()

        var line: String?
        try {

            line = reader.readLine()

            while (line != null) {
                response.append(line)
                line = reader.readLine()
            }

        } catch (e: IOException) {
            Log.e(LOG_TAG, STREAM_ERROR, e)
        } catch (e: Exception) {
            Log.e(LOG_TAG, STREAM_ERROR, e)
        } finally {

            try {
                stream.close()
            } catch (e: IOException) {
                Log.e(LOG_TAG, STREAM_ERROR, e)
            } catch (e: Exception) {
                Log.e(LOG_TAG, STREAM_ERROR, e)
            }

        }
        return response.toString()

    }
}