package cz.filipbursik.bookshelf.utility

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log

object ImageService {

    interface ImageDownloadCallback {
        fun onError()
        fun onSuccess(bitmap: Bitmap?)
    }

    class DownloadImageTask(var imageDownloadCallback: ImageDownloadCallback, var url: String?) : AsyncTask<String, Void, Bitmap>() {

        val LOG_TAG = "Error"

        override fun doInBackground(vararg params: String): Bitmap? {
            var bmp: Bitmap? = null
            try {
                val `in` = java.net.URL(url).openStream()
                bmp = BitmapFactory.decodeStream(`in`)
            } catch (e: Exception) {
                Log.e(LOG_TAG, e.message)
                e.printStackTrace()

                imageDownloadCallback.onError()
            }

            return bmp
        }

        override fun onPostExecute(result: Bitmap?) {
            imageDownloadCallback.onSuccess(result)
        }
    }
}